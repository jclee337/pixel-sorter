# Image Pixel Sorter
This Go program takes an image and sorts its pixels row-by-row by luminosity.

# TODO
This will eventually be a visualizer-style program where you can see the pixels in the image being sorted in realtime.
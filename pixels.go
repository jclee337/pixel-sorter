// This example demonstrates decoding a JPEG image and examining its pixels.
package main

import (
  "strconv"
  "strings"
  "os"
  "path/filepath"
	"image"
  "image/color"
  "image/draw"
	"log"
	"image/png"
	"image/jpeg"
)

func main() {
  var bar string

	// Decode the JPEG data. If reading from file, create a reader with
	reader, err := os.Open(os.Args[1])
	if err != nil {
	  log.Fatal(err)
	}
  filename := filepath.Base(reader.Name())
	defer reader.Close()
	//reader := base64.NewDecoder(base64.StdEncoding, strings.NewReader(data))
	img, format, err := image.Decode(reader)
	if err != nil {
		log.Fatal(err)
	}
	bounds := img.Bounds()

  cimg := image.NewRGBA(bounds)
  draw.Draw(cimg, bounds, img, image.Point{}, draw.Over)

	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for i := bounds.Min.X + 1; i < bounds.Max.X; i++ {
      for j := i; j > bounds.Min.X && compare(cimg.At(j - 1, y), cimg.At(j, y)) > 0; j-- {
        c := cimg.At(j, y)
        cimg.Set(j, y, cimg.At(j - 1, y))
        cimg.Set(j - 1, y, c)
      }
    }
    bar = progress(y + 1, bounds.Max.Y, 60)
    os.Stdout.Write([]byte(bar + "\r"))
    os.Stdout.Sync()
	}

  toimg, _ := os.Create(strings.Replace(filename, ".", "-out.", 1))
  defer toimg.Close()

  switch format {
  case "png":
    png.Encode(toimg, cimg)
  case "jpeg":
    jpeg.Encode(toimg, cimg, &jpeg.Options{100})
  }

  os.Stdout.Write([]byte("\nSorted " + format + "\n"))
}

func compare(c1, c2 color.Color) float64 {
  return colornum(c1) - colornum(c2)
}

func colornum(c color.Color) float64 {
	// A color's RGBA method returns values in the range [0, 65535].
  r, g, b, _ := c.RGBA()
  //return (uint64(r) << 48) | (uint64(g) << 32) | (uint64(b) << 16) | uint64(a)
  return ((float64(r) / 65535.0) * 0.2126) + ((float64(g) / 65535.0) * 0.7152) + ((float64(b) / 65535.0) * 0.0722)
}

func Bold(str string) string {
  return "\033[1m" + str + "\033[0m"
}

func progress(current, total, cols int) string {
  bar_start := " ["
  bar_end := "] "
  suffix := strconv.Itoa(current) + " / " + strconv.Itoa(total)

  bar_size := cols - len(bar_start + bar_end)
  amount := int(float32(current) / (float32(total) / float32(bar_size)))
  remain := bar_size - amount

  bar := strings.Repeat("■", amount) + strings.Repeat("-", remain)
  return  bar_start + bar + bar_end + Bold(suffix)
}
